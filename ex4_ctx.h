#ifndef CTX_H_
#define CTX_H_

#include <petscdm.h>

/* Application context - grid-based data*/
typedef struct LevelCtxData_ {
  DM          dm_stokes,dm_coefficients,dm_faces;
  Vec         coeff;
  PetscInt    cells_x,cells_y,cells_z; /* redundant with DMs */
  PetscReal   hx_characteristic,hy_characteristic,hz_characteristic;
  PetscScalar K_bound,K_cont;
} LevelCtxData;
typedef LevelCtxData* LevelCtx;

/* Application context - problem and grid(s) (but not solver-specific data) */
typedef struct CtxData_ {
  MPI_Comm    comm;
  PetscInt    dim;                       /* redundant with DMs */
  PetscInt    cells_x, cells_y, cells_z; /* Redundant with finest DMs */
  PetscReal   xmax,ymax,xmin,ymin,zmin,zmax;
  PetscScalar eta1,eta2,rho1,rho2,gy,eta_characteristic;
  PetscBool   pin_pressure;
  PetscScalar (*GetEta)(struct CtxData_*,PetscScalar,PetscScalar,PetscScalar);
  PetscScalar (*GetRho)(struct CtxData_*,PetscScalar,PetscScalar,PetscScalar);
  PetscInt    n_levels;
  LevelCtx    *levels;
} CtxData;
typedef CtxData* Ctx;

#endif
