#include "stokes_mars.h"
#include <mars.hpp>
#include <mars_distributed_remote_dof_map.hpp>

#include <petscveckokkos.hpp>
#include <petsc/private/dmstagimpl.h> // for DMStagStencilToIndexLocal

#if defined(USE_TRILINOS_SOLVER)

#include <Tpetra_Core.hpp>
#include <Tpetra_CrsMatrix.hpp>
#include <Tpetra_Vector.hpp>

#include <Kokkos_Macros.hpp>
#include <Kokkos_DefaultNode.hpp>
#include <Tpetra_Core.hpp>
#include <Tpetra_CrsGraph.hpp>
#include <Tpetra_CrsMatrix.hpp>
#include <Tpetra_Map.hpp>
#include <Tpetra_Operator.hpp>
#include <Tpetra_Vector.hpp>

#include <BelosLinearProblem.hpp>
#include <BelosSolverManager.hpp>

#include <Ifpack2_Preconditioner.hpp>

#include <MueLu_TpetraOperator.hpp>
#include <MueLu_Utilities.hpp>
#include <Amesos2_Factory.hpp>
#include <Amesos2_Solver.hpp>
#include <BelosSolverFactory.hpp>
#include <BelosTpetraAdapter.hpp>
#include <MueLu_CreateTpetraPreconditioner.hpp>
#include <Teuchos_ParameterList.hpp>
#include <Teuchos_XMLParameterListCoreHelpers.hpp>

#ifdef KOKKOS_HAVE_OPENMP
typedef Kokkos::Compat::KokkosOpenMPWrapperNode openmp_node;
#endif
#ifdef KOKKOS_HAVE_CUDA
typedef Kokkos::Compat::KokkosCudaWrapperNode cuda_node;
#endif
#ifdef KOKKOS_HAVE_SERIAL
typedef Kokkos::Compat::KokkosSerialWrapperNode serial_node;
#endif
#ifdef KOKKOS_HAVE_PTHREAD
typedef Kokkos::Compat::KokkosThreadsWrapperNode thread_node;
#endif

//~ typedef Kokkos::Compat::KokkosDeviceWrapperNode<Kokkos::Cuda, Kokkos::CudaUVMSpace> NT;   // set the node explicitly
typedef KokkosClassic::DefaultNode::DefaultNodeType NT;  // use the default node for execution (mostly OpenMP or CUDA)

typedef Tpetra::Operator<>::scalar_type SC;
typedef Tpetra::Operator<SC>::local_ordinal_type LO;
typedef Tpetra::Operator<SC, LO>::global_ordinal_type GO;
typedef Tpetra::Map<LO, GO, NT> map_type;
typedef Tpetra::Operator<SC, LO, GO, NT> op_type;
typedef Tpetra::MultiVector<SC, LO, GO, NT> mv_type;
typedef Tpetra::Vector<SC, LO, GO, NT> vec_type;
typedef Tpetra::CrsMatrix<SC, LO, GO, NT> matrix_type;
typedef Tpetra::CrsGraph<LO, GO, NT> graph_type;

typedef Belos::LinearProblem<SC, mv_type, op_type> problem_type;
typedef Belos::SolverManager<SC, mv_type, op_type> solver_type;

typedef Ifpack2::Preconditioner<SC, LO, GO, NT> ifpack_prec_type;

typedef MueLu::TpetraOperator<SC, LO, GO, NT> muelu_prec_type;
typedef MueLu::Utilities<SC, LO, GO, NT> MueLuUtilities;

#endif

static constexpr int IN = 0;
static constexpr int OUT = 1;

template <typename S>
void print_sparsity_pattern(S &sp) {
    const mars::Integer size = sp.get_num_rows();
    Kokkos::parallel_for(
        "for", size, MARS_LAMBDA(const int row) {
            const mars::Integer start = sp.get_row_map(row);
            const mars::Integer end = sp.get_row_map(row + 1);

            // print only if end - start > 0. Otherwise segfaults.
            // The row index is not a global index of the current process!
            for (int i = start; i < end; ++i) {
                auto value = sp.get_value(i);
                auto col = sp.get_col(i);
                // do something. In this case we are printing.

                const mars::Integer local_dof = sp.get_dof_handler().get_owned_dof(row);
                const mars::Integer global_row = sp.get_dof_handler().get_dof_handler().local_to_global(local_dof);

                const mars::Integer local_col = sp.get_dof_handler().global_to_local(col);
                const mars::Integer global_col = sp.get_dof_handler().get_dof_handler().local_to_global(local_col);

                printf("row_dof: %d - %li, col_dof: %li - %li, value: %lf\n",
                       row,
                       global_row,
                       col,
                       global_col,
                       value);
            }
        });
}


template <typename DM>
void set_data_in_circle(const DM &dm, const double a, double b) {
    dm.template owned_data_iterate<IN>(MARS_LAMBDA(const mars::Integer local_dof, double &data) {
        auto octant = dm.get_dof_handler().get_octant_from_local(local_dof);
        double xMax = dm.get_dof_handler().get_XMax();
        double yMax = dm.get_dof_handler().get_YMax();

        double x = octant.x / xMax;
        double y = octant.y / yMax;

        auto r2 = (x - 0.5) * (x - 0.5) + (y - 0.5) * (y - 0.5);

        const double half_width = 0.25;

        if (r2 > half_width * half_width)
            data = a;
        else
            data = b;
    });
}


template <typename S, typename SP>
void assemble_volume(S volume_stencil, SP sp, const mars::Integer proc_num, double hx, double hy, double K_cont, double K_bound) {

    const double dv = hx * hy;

    volume_stencil.iterate(MARS_LAMBDA(const mars::Integer stencil_index) {
        const mars::Integer diag_dof = volume_stencil.get_value(stencil_index, mars::SLabel::Diagonal);

        /* first volume dof of the first process */
        if (proc_num == 0 && stencil_index == 0) {
            sp.set_value(diag_dof, diag_dof, K_bound);
        } else {
            const mars::Integer right_dof = volume_stencil.get_value(stencil_index, mars::SLabel::Right);
            sp.set_value(diag_dof, right_dof, dv * K_cont / hx);

            const mars::Integer left_dof = volume_stencil.get_value(stencil_index, mars::SLabel::Left);
            sp.set_value(diag_dof, left_dof, - dv * K_cont / hx);

            const mars::Integer up_dof = volume_stencil.get_value(stencil_index, mars::SLabel::Up);
            sp.set_value(diag_dof, up_dof, dv * K_cont / hy);

            const mars::Integer down_dof = volume_stencil.get_value(stencil_index, mars::SLabel::Down);
            sp.set_value(diag_dof, down_dof, - dv * K_cont / hy);
        }
    });
}


template <typename S, typename SP, typename DM>
void assemble_face(S face_stencil, SP sp, const DM &dm, double hx, double hy, double K_cont) {
    auto fv_dof_handler = sp.get_dof_handler();

    const double hx2 = hx * hx;
    const double hy2 = hy * hy;
    const double hxhy = hx * hy;
    const double dv = hx * hy;

    face_stencil.iterate(MARS_LAMBDA(const mars::Integer stencil_index) {
        const mars::Integer diag_dof = face_stencil.get_value(stencil_index, mars::SLabel::Diagonal);

        if (!fv_dof_handler.is_boundary_dof(diag_dof)) {
            if (fv_dof_handler.get_orientation(diag_dof) == mars::DofOrient::xDir) {
                auto eta_up_dof = face_stencil.get_value(stencil_index, mars::SSXLabel::CornerXUp);
                auto eta_up_data = dm.template get_dof_data<IN>(eta_up_dof);
                auto eta_down_dof = face_stencil.get_value(stencil_index, mars::SSXLabel::CornerXDown);
                auto eta_down_data = dm.template get_dof_data<IN>(eta_down_dof);

                auto eta_right_dof = face_stencil.get_value(stencil_index, mars::SSXLabel::VolumeXRight);
                auto eta_right_data = dm.template get_dof_data<IN>(eta_right_dof);
                auto eta_left_dof = face_stencil.get_value(stencil_index, mars::SSXLabel::VolumeXLeft);
                auto eta_left_data = dm.template get_dof_data<IN>(eta_left_dof);

                double diag_value = -2.0 * dv * (eta_left_data + eta_right_data) / hx2;

                const mars::Integer pr = face_stencil.get_value(stencil_index, mars::SSXLabel::VolumeXRight);
                sp.set_value(diag_dof, pr, - dv * K_cont / hx);

                const mars::Integer pl = face_stencil.get_value(stencil_index, mars::SSXLabel::VolumeXLeft);
                sp.set_value(diag_dof, pl, dv * K_cont / hx);

                const mars::Integer vxr = face_stencil.get_value(stencil_index, mars::SSXLabel::FaceXRight);
                sp.set_value(diag_dof, vxr, 2.0 * dv * eta_right_data / hx2);

                const mars::Integer vxl = face_stencil.get_value(stencil_index, mars::SSXLabel::FaceXLeft);
                sp.set_value(diag_dof, vxl, 2.0 * dv * eta_left_data / hx2);

                const mars::Integer vxu = face_stencil.get_value(stencil_index, mars::SSXLabel::FaceXUp);
                if (vxu != -1) {
                    diag_value += - dv * eta_up_data / hy2;
                    sp.set_value(diag_dof, vxu, dv * eta_up_data / hy2);
                }

                const mars::Integer vxd = face_stencil.get_value(stencil_index, mars::SSXLabel::FaceXDown);
                if (vxd != -1) {
                    diag_value += - dv * eta_down_data / hy2;
                    sp.set_value(diag_dof, vxd, dv * eta_down_data / hy2);
                }

                const mars::Integer vyur = face_stencil.get_value(stencil_index, mars::SSXLabel::FaceYUpRight);
                sp.set_value(diag_dof, vyur, dv * eta_up_data / hxhy);

                const mars::Integer vyul = face_stencil.get_value(stencil_index, mars::SSXLabel::FaceYUpLeft);
                sp.set_value(diag_dof, vyul, - dv * eta_up_data / hxhy);

                const mars::Integer vydr = face_stencil.get_value(stencil_index, mars::SSXLabel::FaceYDownRight);
                sp.set_value(diag_dof, vydr, - dv * eta_down_data / hxhy);

                const mars::Integer vydl = face_stencil.get_value(stencil_index, mars::SSXLabel::FaceYDownLeft);
                sp.set_value(diag_dof, vydl, dv * eta_down_data / hxhy);

                sp.set_value(diag_dof, diag_dof, diag_value);

            } else if (fv_dof_handler.get_orientation(diag_dof) == mars::DofOrient::yDir) {
                auto eta_up_dof = face_stencil.get_value(stencil_index, mars::SSYLabel::VolumeYUp);
                auto eta_up_data = dm.template get_dof_data<IN>(eta_up_dof);
                auto eta_down_dof = face_stencil.get_value(stencil_index, mars::SSYLabel::VolumeYDown);
                auto eta_down_data = dm.template get_dof_data<IN>(eta_down_dof);

                auto eta_right_dof = face_stencil.get_value(stencil_index, mars::SSYLabel::CornerYRight);
                auto eta_right_data = dm.template get_dof_data<IN>(eta_right_dof);
                auto eta_left_dof = face_stencil.get_value(stencil_index, mars::SSYLabel::CornerYLeft);
                auto eta_left_data = dm.template get_dof_data<IN>(eta_left_dof);

                double diag_value = -2.0 * dv * (eta_up_data + eta_down_data) / hy2;

                const mars::Integer pu = face_stencil.get_value(stencil_index, mars::SSYLabel::VolumeYUp);
                sp.set_value(diag_dof, pu, - dv * K_cont / hy);

                const mars::Integer pd = face_stencil.get_value(stencil_index, mars::SSYLabel::VolumeYDown);
                sp.set_value(diag_dof, pd, dv * K_cont / hy);

                const mars::Integer vyr = face_stencil.get_value(stencil_index, mars::SSYLabel::FaceYRight);
                if (vyr != -1) {
                    diag_value += - dv * eta_right_data / hx2;
                    sp.set_value(diag_dof, vyr, dv * eta_right_data / hx2);
                }
                const mars::Integer vyl = face_stencil.get_value(stencil_index, mars::SSYLabel::FaceYLeft);
                if (vyl != -1) {
                    diag_value += - dv * eta_left_data / hx2;
                    sp.set_value(diag_dof, vyl, dv * eta_left_data / hx2);
                }

                const mars::Integer vyu = face_stencil.get_value(stencil_index, mars::SSYLabel::FaceYUp);
                sp.set_value(diag_dof, vyu, 2.0 * dv * eta_up_data / hy2);

                const mars::Integer vyd = face_stencil.get_value(stencil_index, mars::SSYLabel::FaceYDown);
                sp.set_value(diag_dof, vyd, 2.0 * dv * eta_down_data / hy2);

                const mars::Integer vxur = face_stencil.get_value(stencil_index, mars::SSYLabel::FaceXUpRight);
                sp.set_value(diag_dof, vxur, dv * eta_right_data / hxhy);

                const mars::Integer vxul = face_stencil.get_value(stencil_index, mars::SSYLabel::FaceXUpLeft);
                sp.set_value(diag_dof, vxul, - dv * eta_left_data / hxhy);

                const mars::Integer vxdr = face_stencil.get_value(stencil_index, mars::SSYLabel::FaceXDownRight);
                sp.set_value(diag_dof, vxdr, - dv * eta_right_data / hxhy);

                const mars::Integer vxdl = face_stencil.get_value(stencil_index, mars::SSYLabel::FaceXDownLeft);
                sp.set_value(diag_dof, vxdl, dv * eta_left_data / hxhy);

                sp.set_value(diag_dof, diag_dof, diag_value);
            }
        }
    });
}


template <typename H, typename DM>
mars::ViewVectorType<double> assemble_rhs(const H &fv_dof_handler, const DM &dm, Ctx ctx) {
  mars::ViewVectorType<double> rhs("rhs", fv_dof_handler.get_owned_dof_size());

  // Note that this assumes all boundary values are zero, so no K_bound is passed.

  const double hx = (ctx->xmax - ctx->xmin) / ctx->cells_x;
  const double hy = (ctx->ymax - ctx->ymin) / ctx->cells_y;
  const double gy = ctx->gy;
  const double dv = hx * hy;

  fv_dof_handler.template owned_dof_iterate<mars::DofLabel::lFace>(MARS_LAMBDA(const mars::Integer local_dof) {
      auto o = fv_dof_handler.get_octant_from_local(local_dof);

      if (!fv_dof_handler.is_boundary_dof(local_dof)) {
          double rval = 0;

          if (fv_dof_handler.get_orientation(local_dof) == mars::DofOrient::yDir) {
            auto o1 = mars::Octant(o.x - 1, o.y, o.z);
            auto local_dof1 = fv_dof_handler.get_local_from_octant(o1);

            auto o2 = mars::Octant(o.x + 1, o.y, o.z);
            auto local_dof2 = fv_dof_handler.get_local_from_octant(o2);

            auto avg =
            (dm.template get_dof_data<IN>(local_dof1) + dm.template get_dof_data<IN>(local_dof2)) / 2;
            rval = -1.0 * gy * dv * avg;
          }

          const mars::Integer index = fv_dof_handler.local_to_owned_index(local_dof);
          rhs(index) = rval;
      }
  });

  return rhs;
}


template <typename H, typename V>
void identify_missing_dofs(const H &fv_dof_handler, const V &missing_predicate,
                           PetscInt start[2], PetscInt n[2],
                           PetscInt n_extra[2]) {

  using namespace mars;

  const int st_0 = start[0];
  const int st_1 = start[1];
  const int nn_0 = n[0] + n_extra[0];
  const int n_0 = n[0];
  const int n_1 = n[1];

  Kokkos::parallel_for(
      Kokkos::MDRangePolicy<Kokkos::Rank<2>>(
          {start[1], start[0]},
          {start[1] + n[1] + n_extra[1], start[0] + n[0] + n_extra[0]}),
      MARS_LAMBDA(const mars::Integer ey, const mars::Integer ex) {

        const int i = 3 * ((ex - st_0) + nn_0 * (ey - st_1));

        mars::Octant octant_down(2 * ex + 1, 2 * ey);
        Integer local_sfc_down =
            fv_dof_handler.get_sfc_from_octant(octant_down);
        if (ex < (st_0 + n_0) && !fv_dof_handler.is_local(local_sfc_down)) {
          missing_predicate(i) = 1;
        }

        mars::Octant octant_left(2 * ex, 2 * ey + 1);
        Integer local_sfc_left =
            fv_dof_handler.get_sfc_from_octant(octant_left);
        if (ey < (st_1 + n_1) && !fv_dof_handler.is_local(local_sfc_left)) {
          missing_predicate(i + 1) = 1;
        }

        mars::Octant octant_element(2 * ex + 1, 2 * ey + 1);
        Integer local_sfc_element =
            fv_dof_handler.get_sfc_from_octant(octant_element);
        if (ex < (st_0 + n_0) && ey < (st_1 + n_1) &&
            !fv_dof_handler.is_local(local_sfc_element)) {
          missing_predicate(i + 2) = 1;
        }
      });
}


template <typename H, typename V>
void compact_missing_dofs(const H &fv_dof_handler,
                          const mars::ViewVectorType<bool> &missing_predicate,
                          const V &scan_missing, const V &missing_dofs_sfc,
                          PetscInt start[2], PetscInt n[2],
                          PetscInt n_extra[2]) {

  using namespace mars;

  const int st_0 = start[0];
  const int st_1 = start[1];
  const int nn_0 = n[0] + n_extra[0];
  const int n_0 = n[0];
  const int n_1 = n[1];

  Kokkos::parallel_for(
      Kokkos::MDRangePolicy<Kokkos::Rank<2>>(
          {start[1], start[0]},
          {start[1] + n[1] + n_extra[1], start[0] + n[0] + n_extra[0]}),
      MARS_LAMBDA(const mars::Integer ey, const mars::Integer ex) {
        const int i = 3 * ((ex - st_0) + nn_0 * (ey - st_1));

        if (missing_predicate(i)) {

          mars::Octant octant_down(2 * ex + 1, 2 * ey);
          Integer local_sfc_down =
              fv_dof_handler.get_sfc_from_octant(octant_down);
          Integer index = scan_missing(i);
          missing_dofs_sfc(index) = local_sfc_down;
        }

        if (missing_predicate(i + 1)) {

          mars::Octant octant_left(2 * ex, 2 * ey + 1);
          Integer local_sfc_left =
              fv_dof_handler.get_sfc_from_octant(octant_left);
          Integer index = scan_missing(i + 1);
          missing_dofs_sfc(index) = local_sfc_left;
        }

        if (missing_predicate(i + 2)) {

          mars::Octant octant_element(2 * ex + 1, 2 * ey + 1);
          Integer local_sfc_element =
              fv_dof_handler.get_sfc_from_octant(octant_element);
          Integer index = scan_missing(i + 2);
          missing_dofs_sfc(index) = local_sfc_element;
        }
      });
}


template <typename DOFHandler>
mars::ViewVectorType<mars::Integer>
build_missing_dofs(const DOFHandler &fv_dof_handler, PetscInt start[2],
                   PetscInt n[2], PetscInt n_extra[2],
                   const mars::context &context) {

  using namespace Kokkos;
  using namespace mars;

  int proc_num = rank(context);
  int rank_size = num_ranks(context);

  const int size = 3 * (n[1] + n_extra[1]) * (n[0] + n_extra[0]);
  ViewVectorType<bool> missing_predicate("count_per_proc", size);

  identify_missing_dofs(fv_dof_handler, missing_predicate, start, n, n_extra);

  ViewVectorType<mars::Integer> missing_scan("idx_mars", size + 1);
  incl_excl_scan(0, size, missing_predicate, missing_scan);

  auto index_subview = subview(missing_scan, size);
  auto h_ic = create_mirror_view(index_subview);
  deep_copy(h_ic, index_subview);

  ViewVectorType<Integer> missing_dofs_sfc =
      ViewVectorType<Integer>("missing_sfc_dofs", h_ic());

  compact_missing_dofs(fv_dof_handler, missing_predicate, missing_scan,
                       missing_dofs_sfc, start, n, n_extra);

  return missing_dofs_sfc;
}

template <typename DH, typename RDM>
MARS_INLINE_FUNCTION mars::Integer
get_equation_number(const DH &handler, const RDM &remote_dof_map, const mars::Octant o) {
  using namespace mars;

  const Integer sfc = handler.get_sfc_from_octant(o);
  if (handler.is_local(sfc)) {
    return handler.sfc_to_global(sfc);
  } else {
    return remote_dof_map.sfc_to_global(sfc);
  }
}

extern "C" {

  // Wrapping MARS in an object is too painful, so we get as far as we can
  // with a separate program, and will refactor later once it works, if we have to.
  PetscErrorCode main_MARS(DM dm, Vec *p_x_from_mars, Ctx ctx) {
    PetscErrorCode ierr;
    PetscInt       dim, start[2], N[2], n[2], n_extra[2];
    PetscMPIInt    rank;
    MPI_Comm       comm;
    Vec            rhs_mars;
    VecScatter     mars_to_dmstag_stokes_scatter;

    PetscFunctionBeginUser;
    ierr = PetscObjectGetComm((PetscObject)dm, &comm);CHKERRQ(ierr);
    ierr = MPI_Comm_rank(comm, &rank);CHKERRQ(ierr);
    ierr = DMGetDimension(dm, &dim);
    if (dim !=2) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Printing only implemented for 2D DMStag objects");

    // Global element counts
    ierr = DMStagGetGlobalSizes(dm, &N[0], &N[1], NULL);CHKERRQ(ierr);

    // Get boundaries of native/"owned" elements from DMStag object
    ierr = DMStagGetCorners(dm, &start[0], &start[1], NULL, &n[0], &n[1], NULL, &n_extra[0], &n_extra[1], NULL);CHKERRQ(ierr);

    // Much of this copied/adapted from mars_constant_viscosity_stokes.hpp :

    // Generate a cube with MARS - note that this just partitions the SFC, so
    // even with the same total number of elements, the same element might not be local
    // as with DMStag
    mars::proc_allocation resources;
    auto context = mars::make_context(resources, comm);
    int proc_num = mars::rank(context);
    mars::DistributedQuad4Mesh mesh;
    mars::generate_distributed_cube(context, mesh, N[0], N[1], 0);

    // Enumerate dofs (MARS terminology for unique "locations" in the staggered
    // grid, i.e. corners, faces, and elements in 2D)
    using DHandler =  mars::DofHandler<mars::DistributedQuad4Mesh,2>;
    DHandler dof_handler(&mesh, context);
    dof_handler.enumerate_dofs();

    // Create another dof handler for the faces and elements (volumes)
    using FVDH =  mars::FaceVolumeDofHandler<DHandler>;
    FVDH fv_dof_handler(dof_handler);

    // Create volume and face stencils
    using VStencil =  mars::Stencil<mars::DofLabel::lVolume>;
    auto volume_stencil = mars::build_stencil<VStencil>(fv_dof_handler);
    typedef mars::StokesStencil<mars::DofLabel::lFace> SStencil;
    auto face_stencil = mars::build_stencil<SStencil>(fv_dof_handler);

    // Build a sparsity pattern for a local sparse matrix
    using SPattern =  mars::SparsityPattern<double, default_lno_t, unsigned long, FVDH, VStencil, SStencil>;
    SPattern sp(fv_dof_handler);
    sp.build_pattern(volume_stencil, face_stencil);

    // Build "missing" dof which live on a different rank in the MARS decomposition as the DMStag decomposition
    auto missing_dofs_sfc =
        build_missing_dofs(fv_dof_handler, start, n, n_extra, context);
    mars::RemoteDofMap<FVDH> remote_dof_map(fv_dof_handler);
    remote_dof_map.build_remote_dof_map(missing_dofs_sfc);

    // Data for densities, living on the corners/vertices
    using CDH = mars::CornerDofHandler<DHandler>;
    using CornerDM = mars::SDM<CDH, double>;
    CornerDM cdm(dof_handler);
    set_data_in_circle(cdm, ctx->rho1, ctx->rho2);
    cdm.gather_ghost_data<IN>();
    using CVDH = mars::CornerVolumeDofHandler<DHandler>;
    using CornerVolumeDM = mars::SDM<CVDH, double>;
    CornerVolumeDM cvdm(dof_handler);
    set_data_in_circle(cvdm, ctx->eta1, ctx->eta2);
    cvdm.gather_ghost_data<IN>();

    // Assemble our sparse matrix for the Stokes operator
    double hx = 1.0 / N[0];
    double hy = 1.0 / N[1];
    double K_cont = 1.0;
    double K_bound = 1.0;

    assemble_volume(volume_stencil, sp, proc_num, hx, hy, K_cont, K_bound);
    //assemble_oriented_face(face_stencil, sp, cvdm, hx, hy, K_cont); // potential bug (missing pressure terms in x-mom?)
    assemble_face(face_stencil, sp, cvdm, hx, hy, K_cont);

    fv_dof_handler.boundary_dof_iterate(
        MARS_LAMBDA(const mars::Integer local_dof) { sp.set_value(local_dof, local_dof, K_bound); });

    //print_sparsity_pattern(sp);
    //sp.write("Spattern");

    // Assemble RHS
    auto rhs = assemble_rhs(fv_dof_handler, cdm, ctx);

    // Build a VecScatter object which can convert our solution in MARS's numbering back
    // to a DMStag global Vec. This involves some somewhat-convoluted logic
    // in which we collecte indices on the device and then transfer them back to the
    // host, since PETSc currently only supports defining ISs and VecScatters
    // on the host (though the underlying PetscSF will be defined on the device,
    // when the Vecscatter is used).

    {
      Vec vec_mars, vec_dmstag;
      PetscInt *idx_dmstag;
      PetscInt dmstag_owned_size;
      mars::Integer mars_owned_size;
      IS is_mars, is_dmstag;

      mars_owned_size = fv_dof_handler.get_owned_dof_size();

      ierr = DMGetGlobalVector(dm, &vec_dmstag);CHKERRQ(ierr);
      ierr = VecGetLocalSize(vec_dmstag, &dmstag_owned_size);CHKERRQ(ierr);

      // A PETSc (global) Vec which has the same number of native/owned DOF as are stored by MARS
      ierr = VecCreate(comm, &vec_mars);CHKERRQ(ierr);
      ierr = VecSetSizes(vec_mars, mars_owned_size, PETSC_DECIDE);CHKERRQ(ierr);
      ierr = VecSetType(vec_mars, VECKOKKOS);CHKERRQ(ierr);

      // Both arrays are of the size of the local dmstag (even though MARS has a different owned size)
      ierr = PetscMalloc1(dmstag_owned_size, &idx_dmstag);CHKERRQ(ierr);

      // Note that it's critical that we respect the native DMStag ordering
      // in the way we build the indices from MARS

      mars::ViewVectorType<PetscInt> idx_mars_device("idx_mars", dmstag_owned_size);
      const int mesh_size = 3 * (n[1] + n_extra[1]) * (n[0] + n_extra[0]);
      mars::ViewVectorType<bool> idx_mars_predicate("idx_mars", mesh_size);

      auto idx_mars_host = Kokkos::create_mirror_view(idx_mars_device);

        const int st_0 = start[0];
        const int st_1 = start[1];
        const int nn_0 = n[0] + n_extra[0];

        Kokkos::parallel_for(Kokkos::MDRangePolicy<Kokkos::Rank<2>>({start[1], start[0]}, {start[1] + n[1] + n_extra[1], start[0] + n[0] + n_extra[0]}),
            MARS_LAMBDA(const mars::Integer ey, const mars::Integer ex) {

              const int index = 3 * ((ex - st_0) + nn_0 * (ey - st_1));

              if (ex < start[0] + n[0]) {
                idx_mars_predicate(index) = 1;
              }

              if (ey < start[1] + n[1]) {
                idx_mars_predicate(index + 1) = 1;
              }

              if (ex < start[0] + n[0] && ey < start[1] + n[1]) {
                idx_mars_predicate(index + 2) = 1;
              }
            });

        mars::ViewVectorType<mars::Integer> idx_mars_scan("idx_mars", mesh_size+1);
        mars::incl_excl_scan(0, mesh_size, idx_mars_predicate, idx_mars_scan);


        Kokkos::parallel_for(Kokkos::MDRangePolicy<Kokkos::Rank<2>>({start[1], start[0]}, {start[1] + n[1] + n_extra[1], start[0] + n[0] + n_extra[0]}),
            MARS_LAMBDA(const mars::Integer ey, const mars::Integer ex) {

            const mars::Integer index = 3 * ((ex - st_0) + nn_0 * (ey - st_1));

            if(idx_mars_predicate(index)) {
            mars::Octant octant_down(2*ex+1, 2*ey);
            mars::Integer local_dof_down = fv_dof_handler.get_local_from_octant(octant_down);
            mars::Integer global_dof_down = fv_dof_handler.get_dof_handler().local_to_global(local_dof_down);
            //mars::Integer equation_number_down = fv_dof_handler.local_to_global(local_dof_down);
            mars::Integer equation_number_down =
                get_equation_number(fv_dof_handler, remote_dof_map, octant_down);

            const mars::Integer i = idx_mars_scan(index);
            idx_mars_device(i) = equation_number_down;
            }

            if (idx_mars_predicate(index + 1)) {
            mars::Octant octant_left(2*ex, 2*ey+1);
            mars::Integer local_dof_left = fv_dof_handler.get_local_from_octant(octant_left);
            mars::Integer global_dof_left = fv_dof_handler.get_dof_handler().local_to_global(local_dof_left);
            //mars::Integer equation_number_left = fv_dof_handler.local_to_global(local_dof_left);
            mars::Integer equation_number_left =
                get_equation_number(fv_dof_handler, remote_dof_map, octant_left);

            const mars::Integer i = idx_mars_scan(index + 1);
            idx_mars_device(i) = equation_number_left;
            }

            if (idx_mars_predicate(index + 2)) {
              mars::Octant octant_element(2*ex+1, 2*ey+1);
              mars::Integer local_dof_element = fv_dof_handler.get_local_from_octant(octant_element);
              mars::Integer global_dof_element = fv_dof_handler.get_dof_handler().local_to_global(local_dof_element);
              //mars::Integer equation_number_element = fv_dof_handler.local_to_global(local_dof_element);
              mars::Integer equation_number_element =
                  get_equation_number(fv_dof_handler, remote_dof_map, octant_element);

              const mars::Integer i = idx_mars_scan(index + 2);
              idx_mars_device(i) = equation_number_element;
            }
            });

      //copy the data from the device to host for using it with PETSc thereafter.
      Kokkos::deep_copy(idx_mars_host, idx_mars_device);

      // Check that the parallel decompositions are similar enough for the current
      // approach to work. This currently fails if the ghost region for MARS doesn't
      // overlap the native (owned) region for DMStag/PETSc.
      ierr = PetscPrintf(comm, "Checking decomposition\n");CHKERRQ(ierr);
      for (int i = 0; i<dmstag_owned_size; ++i) {
        if (idx_mars_host(i) == -1) SETERRABORT(comm,PETSC_ERR_SUP,"-1 Index detected. Parallel decompositions are not similar enough.");
      }

      // Note that the size of this is from DMStag
      ierr = ISCreateGeneral(comm, dmstag_owned_size, idx_mars_host.data(), PETSC_USE_POINTER, &is_mars);CHKERRQ(ierr);

      ierr = VecScatterCreate(vec_mars, is_mars, vec_dmstag, NULL, &mars_to_dmstag_stokes_scatter);CHKERRQ(ierr);

      ierr = ISDestroy(&is_mars);CHKERRQ(ierr);
      ierr = VecDestroy(&vec_mars);CHKERRQ(ierr);
      ierr = DMRestoreGlobalVector(dm, &vec_dmstag);CHKERRQ(ierr);

    }

    // Solve the System created above
    {
      Vec x_mars;

#if defined(USE_TRILINOS_SOLVER)
    //Tpetra::ScopeGuard tpetraScope (&argc, &argv);
    Tpetra::ScopeGuard tpetraScope ((int*)0, (char***)0);
    {
      auto comm = Tpetra::getDefaultComm ();
      const int myRank = comm->getRank ();

      using LO = Tpetra::Map<>::local_ordinal_type;
      using GO = Tpetra::Map<>::global_ordinal_type;

      LO numLclRows = fv_dof_handler.get_owned_dof_size();
      GO numGblRows = fv_dof_handler.get_global_dof_size();


      using Teuchos::RCP;
      using Teuchos::rcp;

      const GO indexBase = 0;
      RCP<const Tpetra::Map<> > rowMap =
        rcp (new Tpetra::Map<> (numGblRows, numLclRows, indexBase, comm));
      // since MARS provides already global indices for the columns we can just use the identity map, replicated on each process
      RCP<const Tpetra::Map<>> colMap =
        rcp(new Tpetra::Map<>(numGblRows, indexBase, comm, Tpetra::LocallyReplicated));


      // this constructor gives us a fill-complete matrix, so do not call fillComplete manually again
      Teuchos::RCP<matrix_type> A = Teuchos::rcp(new matrix_type(sp.get_matrix(), rowMap, colMap)); //, rowOffsets, colIndices, matrixValues);
      //        A.fillComplete ();  // do not call fillComplete when using the constructor with Kokkos::CrsMatrix as first argument, this is a fillComplete-constructor

      // Hack to deal with the fact that Tpetra::Vector needs a
      // DualView<double**> for now, rather than a View<double*>. // AF: is this comment actually still valid? I need to investigate this a bit more I guess
      Kokkos::DualView<double**, Kokkos::LayoutLeft> rhs_lcl ("b", numLclRows, 1);
      rhs_lcl.modify<Kokkos::DualView<double**, Kokkos::LayoutLeft>::t_dev::execution_space> ();
      Kokkos::deep_copy (Kokkos::subview (rhs_lcl.d_view, Kokkos::ALL (), 0), rhs);
      //
      Kokkos::DualView<double**, Kokkos::LayoutLeft> lhs_lcl ("x", numLclRows, 1);
      // uncomment to start with a non-zero initial guess, fill it with whatever initial guess seems approriate (the solution itself would be a good initial guess)
      //        lhs_lcl.modify<Kokkos::DualView<double**, Kokkos::LayoutLeft>::t_dev::execution_space>();
      //        Kokkos::deep_copy (Kokkos::subview (lhs_lcl.d_view, Kokkos::ALL (), 0), rhs);

      Teuchos::RCP<vec_type> RHS = Teuchos::rcp(new vec_type(A->getRangeMap(), rhs_lcl));
      Teuchos::RCP<vec_type> LHS = Teuchos::rcp(new vec_type(A->getDomainMap(), lhs_lcl));

      //  A->describe(*Teuchos::fancyOStream(Teuchos::rcpFromRef(std::cout)), Teuchos::VERB_EXTREME);
      //        RHS->describe(*Teuchos::fancyOStream(Teuchos::rcpFromRef(std::cout)), Teuchos::VERB_EXTREME);
      //        LHS->describe(*Teuchos::fancyOStream(Teuchos::rcpFromRef(std::cout)), Teuchos::VERB_EXTREME);

      Teuchos::RCP<Belos::LinearProblem<SC, mv_type, op_type> > linearProblem =
        Teuchos::rcp(new problem_type(A, LHS, RHS));

      //        std::cout << "rhs=(";
      //        for (size_t i=0; i<numLclRows; ++i) {
      //            std::cout << rhs(i) << ", ";
      //        }
      //        std::cout << ")" << std::endl;

      //        std::cout << "rhs.size()=" << rhs.size() << " numLclRows=" << numLclRows << std::endl;
      //        std::cout << "A.frobeniusNorm=" << A->getFrobeniusNorm() << std::endl;

      linearProblem->setProblem();

      std::string param_file_name = "solver_config.xml";
      static Teuchos::RCP<Teuchos::ParameterList> ParamList =
        Teuchos::getParametersFromXmlFile(param_file_name);
      auto& mainPL = ParamList->sublist("main", true);

      bool direct_solver = mainPL.get("Direct Solver", false);
      std::string preconditioner_type = mainPL.get<std::string>("Preconditioner Type", "none");
      std::string dir_prec_type = mainPL.get("Ifpack2 Preconditioner", "prec_type_unset");
      std::string sol_type = mainPL.get("Solver Type", "CG");
      //~ std::string dir_sol_type = "SuperLU";


      //        Teuchos::RCP<Amesos2::Solver<matrix_type, mv_type> > directSolver;
      Teuchos::RCP<solver_type> belosSolver;

      if (false==direct_solver) {
        {
          Belos::SolverFactory<SC, mv_type, op_type> belosFactory;
          //            if (myRank==0) {
          //                std::cout << "Belos supported solvers: ";
          //                for (const auto &s : belosFactory.supportedSolverNames()) {
          //                    std::cout << s << ", ";
          //                }
          //                std::cout << std::endl;
          //            }
          auto belosParams = Teuchos::sublist(ParamList, sol_type, false);
          belosSolver = belosFactory.create(sol_type, belosParams);
        }
        assert(!belosSolver.is_null());

        Teuchos::RCP<ifpack_prec_type> M_ifpack;
        Teuchos::RCP<muelu_prec_type> M_muelu;
        if ("none" == preconditioner_type) {
          // nothing to setup if we do not want to use a preconditioner
        }
        else if ("ifpack2" == preconditioner_type) {
          M_ifpack = Ifpack2::Factory::create<matrix_type>(dir_prec_type, A);
          assert(!M_ifpack.is_null());
          M_ifpack->setParameters(ParamList->sublist(dir_prec_type, false));
          M_ifpack->initialize();
          M_ifpack->compute();
          linearProblem->setRightPrec(M_ifpack);
        } else if ("muelu" == preconditioner_type) {
          // Multigrid Hierarchy
          M_muelu = MueLu::CreateTpetraPreconditioner((Teuchos::RCP<op_type>)A,
              ParamList->sublist("MueLu", false));
          assert(!M_muelu.is_null());
          linearProblem->setRightPrec(M_muelu);
        } else {
          std::cerr << "Specified an invalid 'Preconditioner Type'. Known values: 'none', 'ifpack2', 'muelue'. Provided value: '" << preconditioner_type << "'" << std::endl;
          exit(1);
        }

        // linearProblem->setProblem ();
        belosSolver->setProblem(linearProblem);

        Belos::ReturnType belosResult;
        belosResult = belosSolver->solve();

        int numIterations = belosSolver->getNumIters();
        double residualOut = belosSolver->achievedTol();
        if (myRank == 0) {
          std::cout << "number of iterations = " << numIterations << std::endl;
          std::cout << "||Residual|| = " << residualOut << std::endl;
        }

        Kokkos::deep_copy(rhs, Kokkos::subview (lhs_lcl.d_view, Kokkos::ALL (), 0));

        //          RHS->describe(*Teuchos::fancyOStream(Teuchos::rcpFromRef(std::cout)), Teuchos::VERB_EXTREME);
        //          LHS->describe(*Teuchos::fancyOStream(Teuchos::rcpFromRef(std::cout)), Teuchos::VERB_EXTREME);
        //          std::cout << "rhs (deep copied from solution)=(";
        //          for (size_t i=0; i<numLclRows; ++i) {
        //              // this does not work for the GPU!
        //              std::cout << (std::abs(rhs(i))<1e-12 ? 0 : rhs(i)) << ", ";
        //          }
        //          std::cout << ")" << std::endl;
      } else {
        std::cerr << "Direct solver not supported yet..." << std::endl;
        exit(1);
        //          direct_solver = true;
        //          directSolver =
        //              Amesos2::create<matrix_type, mv_type>(sol_type, A, lhs_lcl, rhs_lcl);
        //          //~ Teuchos::writeParameterListToXmlOStream(*directSolver->getValidParameters(), std::cout);
        //          Teuchos::RCP<Teuchos::ParameterList> amesos2params(new Teuchos::ParameterList("Amesos2"));
        //          amesos2params->set(sol_type, ParamList->sublist(sol_type, false));
        //          directSolver->setParameters(amesos2params);
        //          {
        //            directSolver->solve();
        //          }
        //          //~ Teuchos::ParameterList timings;
        //          //~ directSolver->getTiming(timings);
        //          //~ Teuchos::writeParameterListToXmlOStream(timings, std::cout);
      }
    }

    // Scatter rhs, which now contains the solution, to our PETSc-ordered Vec

    // Suboptimal - copy brute force to a Petsc Vec
    {
      PetscScalarViewDevice_t x_view_d;

      mars::Integer mars_owned_size = fv_dof_handler.get_owned_dof_size();

      ierr = VecCreate(comm, &x_mars);CHKERRQ(ierr);
      ierr = VecSetSizes(x_mars, mars_owned_size, PETSC_DECIDE);CHKERRQ(ierr);
      ierr = VecSetType(x_mars, VECKOKKOS);CHKERRQ(ierr);

      ierr = VecKokkosGetDeviceView(x_mars, &x_view_d);CHKERRQ(ierr);

      Kokkos::parallel_for("naive_copy", mars_owned_size, MARS_LAMBDA(const int i) {
          x_view_d(i) = rhs(i);
        });

      ierr = VecKokkosRestoreDeviceView(x_mars, &x_view_d);CHKERRQ(ierr);
    }
#else
      // Test - instead of solving, populate solution vector with known function
      PetscScalarViewDevice_t x_view_d;

      mars::Integer mars_owned_size = fv_dof_handler.get_owned_dof_size();

      ierr = VecCreate(comm, &x_mars);CHKERRQ(ierr);
      ierr = VecSetSizes(x_mars, mars_owned_size, PETSC_DECIDE);CHKERRQ(ierr);
      ierr = VecSetType(x_mars, VECKOKKOS);CHKERRQ(ierr);

      ierr = VecKokkosGetDeviceView(x_mars, &x_view_d);CHKERRQ(ierr);
      fv_dof_handler.template owned_dof_iterate<mars::DofLabel::lFace>(MARS_LAMBDA(const mars::Integer local_dof) {
          double val = 0;

          if (!fv_dof_handler.is_boundary_dof(local_dof)) {
            auto orientation = fv_dof_handler.get_orientation(local_dof);
            if (orientation == mars::DofOrient::yDir) {
              val = 2.0;
            } else if (orientation == mars::DofOrient::xDir) {
              val = 3.0;
            }

            const mars::Integer index = fv_dof_handler.local_to_owned_index(local_dof);
            x_view_d(index) = val;
          }
      });

      fv_dof_handler.template owned_dof_iterate<mars::DofLabel::lVolume>(MARS_LAMBDA(const mars::Integer local_dof) {
          double val = 0;
          val = 4.0;
          const mars::Integer index = fv_dof_handler.local_to_owned_index(local_dof);
          x_view_d(index) = val;
      });
      ierr = VecKokkosRestoreDeviceView(x_mars, &x_view_d);CHKERRQ(ierr);
#endif

      // Scatter to reorder
      ierr = DMCreateGlobalVector(dm, p_x_from_mars);CHKERRQ(ierr);
      ierr = VecScatterBegin(mars_to_dmstag_stokes_scatter, x_mars, *p_x_from_mars, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
      ierr = VecScatterEnd(mars_to_dmstag_stokes_scatter, x_mars, *p_x_from_mars, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
      ierr = VecDestroy(&x_mars);CHKERRQ(ierr);
    }

    PetscFunctionReturn(0);
  }

  PetscErrorCode MARSInitialize(int argc, char** argv) {
    PetscFunctionBeginUser;
    // MARS should namespace this:
#ifdef WITH_KOKKOS
    Kokkos::initialize(argc, argv);
    // Note mars::Env sets a maximum CUDA stack size, which we don't here
#endif
    PetscFunctionReturn(0);
  }

  PetscErrorCode MARSFinalize() {
    PetscFunctionBeginUser;
    // MARS should namespace this:
#ifdef WITH_KOKKOS
    Kokkos::finalize();
#endif
    PetscFunctionReturn(0);
  }


} // extern "C"
