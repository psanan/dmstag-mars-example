# Demo for StagBL/DMStag + MARS

A simple example to work with in attempts to integrate an example from the
StagBL project, relying on [PETSc's](https://www.mcs.anl.gov/petsc/) DMStag staggered-grid abstraction,
with [MARS](https://bitbucket.org/zulianp/mars), a library for performance-portable mesh management based on
space-filling curves (SFCs).

Recipe for main example case:

* [Piz Daint, GPU](docs/recipe_daint_gpu.md)

Additional recipes, used for development purposes:

* [Piz Daint, CPU](docs/recipe_daint_cpu.md)
* [Local Ubuntu machine, CPU](docs/recipe_local_openmp.md)
