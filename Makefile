# This GNU Makefile is modified from $PETSC_DIR/share/petsc/Makefile.user

EX4_MOD_NAME = ex4_mars

all: $(EX4_MOD_NAME)


################################################################################

ifndef PETSC_DIR
	$(error To use PETSc, PETSC_DIR and maybe PETSC_ARCH must be defined in your Makefile or environment)
endif

# Obtain most flags and compilers from PETSc's configuration
petsc.pc := $(PETSC_DIR)/$(PETSC_ARCH)/lib/pkgconfig/petsc.pc

PACKAGES := $(petsc.pc)

CC := $(shell pkg-config --variable=ccompiler $(PACKAGES))
USE_NVCC_WRAPPER=1
ifeq ($(USE_NVCC_WRAPPER),1)
  CXX = $(TRILINOS_DIR)/bin/nvcc_wrapper
else
  CXX := $(shell pkg-config --variable=cxxcompiler $(PACKAGES))
endif
FC := $(shell pkg-config --variable=fcompiler $(PACKAGES))
CFLAGS_OTHER := $(shell pkg-config --cflags-only-other $(PACKAGES))
CFLAGS_DEP := $(shell pkg-config --variable=cflags_dep $(PACKAGES))
CFLAGS := $(shell pkg-config --variable=cflags_extra $(PACKAGES)) $(CFLAGS_OTHER) $(CFLAGS_DEP)
CXXFLAGS := $(shell pkg-config --variable=cxxflags_extra $(PACKAGES)) $(CFLAGS_OTHER) $(CFLAGS_OTHER)
FFLAGS := $(shell pkg-config --variable=fflags_extra $(PACKAGES))
CPPFLAGS := $(shell pkg-config --cflags-only-I $(PACKAGES))
LDFLAGS := $(shell pkg-config --libs-only-L --libs-only-other $(PACKAGES))
LDFLAGS += $(patsubst -L%, $(shell pkg-config --variable=ldflag_rpath $(PACKAGES))%, $(shell pkg-config --libs-only-L $(PACKAGES)))
LDLIBS := $(shell pkg-config --libs-only-l $(PACKAGES)) -lm
CUDAC := $(shell pkg-config --variable=cudacompiler $(PACKAGES))
CUDAC_FLAGS := $(shell pkg-config --variable=cudaflags_extra $(PACKAGES))
CUDA_LIB := $(shell pkg-config --variable=cudalib $(PACKAGES))
CUDA_INCLUDE := $(shell pkg-config --variable=cudainclude $(PACKAGES))

################################################################################


EX4_MOD.o=$(EX4_MOD_NAME).o stokes_mars.o

$(EX4_MOD_NAME) : $(EX4_MOD.o)
	$(LINK.cc) -o $@ $^ $(LDLIBS)

MARS_DIR=$(HOME)/code/mars-install
MARS_INCLUDE=-I$(MARS_DIR)/include
MARS_LIBS=-L$(MARS_DIR)/lib -lmars_mpi -lmars # there are other libraries which you may need

# Expect TRILINOS_DIR in the environment, as in our Piz Daint env
TRILINOS_DIR ?= /path/to/trilinos

KOKKOS_DIR=$(TRILINOS_DIR)
KOKKOS_INCLUDE = -I$(KOKKOS_DIR)/include
KOKKOS_LIBS = -L$(KOKKOS_DIR)/lib -lkokkoskernels -lkokkoscontainers -lkokkoscore

LDLIBS+=$(MARS_LIBS) $(KOKKOS_LIBS) -lstdc++

# It is of course a bad idea to suppress warning flags
CXXFLAGS_EXTRA_MARS := \
					-std=c++14 \
					-Wno-sign-compare \
					-Wno-unused-variable \
					-Wno-return-type \
					-Wno-reorder \
					-Wno-comment \
					-Wno-unused-local-typedefs \

USE_TRILINOS_SOLVER = 1
TRILINOS_SOLVER_DIR=$(TRILINOS_DIR)
ifeq ($(USE_TRILINOS_SOLVER),1)
  CXXFLAGS_EXTRA_MARS += -DUSE_TRILINOS_SOLVER
  TRILINOS_SOLVER_LIBS = -L$(TRILINOS_SOLVER_DIR) -lamesos2 -lbelostpetra -lbelos -lifpack2 -lmuelu -lmuelu-interface -lxpetra-sup -lxpetra -ltpetra -lteuchoscomm -lteuchosparameterlist -lteuchoscore -lesmumps -lptscotch -lptscotcherr -lscotch
  LDLIBS+=$(TRILINOS_SOLVER_LIBS)
  TRILINOS_SOLVER_INCLUDE = -I$(TRILINOS_SOLVER_DIR)/include
endif

stokes_mars.o : stokes_mars.cxx
	$(COMPILE.cc) $(CXXFLAGS_EXTRA_MARS) $(MARS_INCLUDE) $(KOKKOS_INCLUDE) $(TRILINOS_SOLVER_INCLUDE) $(OUTPUT_OPTION) $<

clean_output:
	$(RM) -f *.vtr Spattern* *.pbin *.pbin.info

clean : clean_output
	$(RM) $(EX4_MOD_NAME) $(EX4_MOD.o) $(EX4_MOD.d)

################################################################################

EX4_MOD.d := $(EX4_MOD.o:%.o=%.d)

# Tell make that .d files are all up to date.  Without this, the include
# below has quadratic complexity
$(EX4_MOD.d) : ;

-include $(EX4_MOD.d)
