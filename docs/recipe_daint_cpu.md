# Recipe: Piz Daint (CPU)

## Load environment and set convenience function to run

    source $APPS/UES/anfink/cpu/environment
    unset PETSC_DIR
    function my_srun() { 
      srun -C gpu -l -A sm50 -p debug "$@" 
    }

## Build MARS

    mkdir -p $HOME/code
    cd $HOME/code
    git clone --recurse-submodules https://bitbucket.org/zulianp/mars -b stagbl
    cd mars
    mkdir build-cpu && cd build-cpu
    cmake \
      -DCMAKE_VERBOSE_MAKEFILE=ON \
      -DCMAKE_BUILD_TYPE=Debug \
      -DTRY_WITH_KOKKOS=ON \
      -DMARS_USE_CUDA=OFF \
      -DCMAKE_INSTALL_PREFIX=$HOME/code/mars-cpu-install \
      ..
    make install

## Run MARS example to test

    cd $SCRATCH
    my_srun \
      -n 4 \
      $HOME/code/mars-cpu-install/bin/mars_exec \
        -l 4 \
        -a vstokes

## Run MARS solver example

The long compile line is copied from the top of `mars_staggered.cpp`:

    cd $HOME/code
    git clone https://bitbucket.org/dganellari/mars-tutorials
    cd mars-tutorials/examples
    (
      export PETSC_DIR=$APPS/UES/anfink/stagbl/petsc.cpu
      export LD_LIBRARY_PATH=$PETSC_DIR:$LD_LIBRARY_PATH
      export MARS_DIR=$HOME/code/mars-cpu-install
      CC -O0 -fopenmp -DNDEBUG -std=c++14 -I$MARS_DIR/include -I$PETSC_DIR/include -I$TRILINOS_DIR/include -L$TRILINOS_DIR/lib -L$MARS_DIR/lib -L$PETSC_DIR/lib -o mars_staggered ../examples/mars_staggered.cpp -lamesos2 -lbelostpetra -lbelos -lifpack2 -lmuelu -lmuelu-interface -lxpetra-sup -lxpetra -ltpetra -lteuchoscomm -lteuchosparameterlist -lteuchoscore -lmars_core -lmars_mpi -lmars -lkokkoscontainers -lkokkoskernels -lkokkoscore -lesmumps -lptscotch -lptscotcherr -lscotch
    )
    my_srun -n 4 ./mars_staggered

## Build and Test our demo code


    cd $SCRATCH
    git clone https://gitlab.com/psanan/dmstag-mars-example  # this repository
    cd dmstag-mars-example
    (
      export PETSC_DIR=$APPS/UES/anfink/stagbl/petsc.cpu
      export LD_LIBRARY_PATH=$PETSC_DIR:$LD_LIBRARY_PATH
      make MARS_DIR=$HOME/code/mars-cpu-install
    )
    my_srun -n 4  ./ex4_mars