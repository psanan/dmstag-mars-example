# Recipe: Piz Daint

## Load environments and define convenience function to run

These were set up by Andreas Fink, and includes a build of
PETSc on a working branch, named `psanan/stagbl-working-base-rebase` 
and configured `--with-cuda --with-kokkos --with-kokkos-kernels`
(See the end of this document for more information). Note that
the environment sets a different, default `PETSC_DIR`, which we unset.

    source $APPS/UES/anfink/gpu/environment
    unset PETSC_DIR
    function my_srun() {
       export CUDA_LAUNCH_BLOCKING=1
       export MPICH_GNI_LMT_PATH=disabled
       export MPICH_RDMA_ENABLED_CUDA=1
       export CRAY_CUDA_MPS=1
       export OMP_PROC_BIND=false
       srun -C gpu -l -A sm50 -p debug "$@"
    }

## Build MARS

With the environment loaded above, you can use CMake as described in the [MARS readme](ttps://bitbucket.org/zulianp/mars)

    mkdir -p $HOME/code
    cd $HOME/code
    git clone --recurse-submodules https://bitbucket.org/zulianp/mars -b stagbl
    cd mars
    mkdir build && cd build
    cmake \
      -DCMAKE_VERBOSE_MAKEFILE=ON \
      -DCMAKE_BUILD_TYPE=Release \
      -DTRY_WITH_KOKKOS=ON \
      -DMARS_USE_CUDA=ON \
      -DMARS_USE_CUDAUVM=ON \
      -DCMAKE_INSTALL_PREFIX=$HOME/code/mars-install \
      ..
    make install

## Run MARS solver example (optional)

The long compile line is copied from the top of `mars_staggered.cpp`:

    cd $HOME/code
    git clone https://bitbucket.org/dganellari/mars-tutorials
    cd mars-tutorials/examples
    (
      export PETSC_DIR=$APPS/UES/anfink/stagbl/petsc
      export LD_LIBRARY_PATH=$PETSC_DIR:$LD_LIBRARY_PATH
      export MARS_DIR=$HOME/code/mars-install
      $TRILINOS_DIR/bin/nvcc_wrapper -O0 -fopenmp -DNDEBUG -std=c++14 -I$MARS_DIR/include -I$PETSC_DIR/include -I$TRILINOS_DIR/include -L$TRILINOS_DIR/lib -L$MARS_DIR/lib -L$PETSC_DIR/lib -o mars_staggered ../examples/mars_staggered.cpp -lamesos2 -lbelostpetra -lbelos -lifpack2 -lmuelu -lmuelu-interface -lxpetra-sup -lxpetra -ltpetra -lteuchoscomm -lteuchosparameterlist -lteuchoscore -lmars_core -lmars_mpi -lmars -lkokkoscontainers -lkokkoskernels -lkokkoscore -lesmumps -lptscotch -lptscotcherr -lscotch 2>&1 | tee out.txt
    )
    my_srun -n 1 ./mars_staggered
    my_srun -n 4 ./mars_staggered

## Build and Test our demo code

    cd $SCRATCH
    git clone https://gitlab.com/psanan/dmstag-mars-example  # this repository
    cd dmstag-mars-example
    (
      export PETSC_DIR=$APPS/UES/anfink/stagbl/petsc
      export LD_LIBRARY_PATH=$PETSC_DIR:$LD_LIBRARY_PATH
      make MARS_DIR=$HOME/code/mars-install
    )
    my_srun -n 1  ./ex4_mars
    my_srun -n 4  ./ex4_mars
    my_srun -n 4  ./ex4_mars -stag_grid_x 64 -stag_grid_y 64
    my_srun -n 16  ./ex4_mars -stag_grid_x 64 -stag_grid_y 64


If you open the generated `ex4_element.vtr` in Paraview and manipulate the data,
you should be able to generate a picture like this. 
There is a Paraview state file [here](images/2021-03-03.pvsm) which might help generate this)

  ![image](images/2021-03-03_ex4_64x64.png)


# Versions and additional information, 2021-02-03 

Additional information for reproducibility purposes.

## Git repository commmits

*  MARS: `02f9229fd23c48458768e95e25cf890e5b151b55` at https://bitbucket.org/zulianp/mars (branch `stagbl`)
*  Mars tutorials `dac88cbcec938a7076302c59be63b0e29a85e3df` at https://bitbucket.org/dganellari/mars-tutorials (branch `stagbl`)

## Environment

`$APPS/UES/anfink/gpu/environment`:
```
module purge >/dev/null 2>&1
module load modules
module load PrgEnv-gnu
module load daint-gpu
module load cray-mpich
module load cray-hdf5-parallel
module load cray-netcdf-hdf5parallel
module load CMake
module load cray-libsci
module load VTK
module load cudatoolkit
export CRAYPE_LINK_TYPE=dynamic
export CC=cc
export CXX=CC
export FC=ftn
export F90=ftn
export F77=ftn
CXXFLAGS="$(printf '%s\n' "${CXXFLAGS} -std=c++11" | awk -v RS='[[:space:]]+' '!a[$0]++{printf "%s%s", $0, RT}')"
export CXXFLAGS
export P4EST_DIR="/apps/daint/UES/anfink/gpu_20200910_Release//p4est"
export BOOST_DIR="/apps/daint/UES/anfink/gpu_20200910_Release//boost"
export EIGEN_DIR="/apps/daint/UES/anfink/gpu_20200910_Release//eigen"
export TRILINOS_DIR="/apps/daint/UES/anfink/gpu_20200910_Release//trilinos"
export PETSC_DIR="/apps/daint/UES/anfink/gpu_20200910_Release//petsc"
export LIBMESH_DIR="/apps/daint/UES/anfink/gpu_20200910_Release//libmesh"
export MOOSE_DIR="/apps/daint/UES/anfink/gpu_20200910_Release//moose"
export METHOD="opt"
export MOONOLITH_DIR="/apps/daint/UES/anfink/gpu_20200910_Release//moonolith"
export UTOPIA_DIR="/apps/daint/UES/anfink/gpu_20200910_Release//utopia"
export LD_LIBRARY_PATH="${P4EST_DIR}/lib:${TRILINOS_DIR}/lib:${PETSC_DIR}/lib:${MOOSE_DIR}/framework:${LIBMESH_DIR}/lib:$LD_LIBRARY_PATH"
```

## PETSc

From `$APPS/UES/anfink/stagbl/petsc/build.log`

```
PETSC_GIT_REPO=https://bitbucket.org/psanan/petsc
#define PETSC_VERSION_BRANCH_GIT "psanan/stagbl-working-base-rebase"
#define PETSC_VERSION_DATE_GIT "2021-02-12 18:03:10 +0100"
#define PETSC_VERSION_GIT "v3.6.3-24842-g7480710889"
```

`$APPS/UES/anfink/stagbl/petsc/lib/petsc/conf/reconfigure-arch-linux-c-opt.py`
 ```
#!/opt/python/3.8.2.1/bin/python3
if __name__ == '__main__':
  import sys
  import os
  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
    '--CFLAGS=-O3 -march=native',
    '--CUDAFLAGS=--compiler-bindir=CC',
    '--CXXFLAGS=-O3 -march=native',
    '--download-hypre=1',
    '--download-metis=1',
    '--download-mumps=1',
    '--download-parmetis=1',
    '--download-ptscotch=1',
    '--download-scalapack=1',
    '--download-suitesparse=1',
    '--download-sundials-configure-arguments=--with-mpicc=cc --enable-cvodes --enable-ida --enable-idas --enable-kinsol',
    '--download-sundials=1',
    '--download-superlu=1',
    '--download-superlu_dist-cmake-arguments=-DTPL_BLAS_LIBRARIES="-lblas" -DTPL_LAPACK_LIBRARIES="-llapack"',
    '--download-superlu_dist=1',
    '--prefix=/apps/daint/UES/anfink/stagbl/petsc',
    '--with-batch=1',
    '--with-cc=cc',
    '--with-cuda-dir=/opt/nvidia/cudatoolkit10.2/10.2.89_3.28-7.0.2.1_2.17__g52c0314',
    '--with-cuda=1',
    '--with-cxx-dialect=C++14',
    '--with-cxx=CC',
    '--with-debugging=0',
    '--with-fc=ftn',
    '--with-hdf5=1',
    '--with-kokkos-cuda-arch=sm_60',
    '--with-kokkos-dir=/apps/daint/UES/anfink/gpu_20200910_Release//trilinos',
    '--with-kokkos-kernels-dir=/apps/daint/UES/anfink/gpu_20200910_Release//trilinos',
    '--with-kokkos-kernels=1',
    '--with-kokkos=1',
    '--with-mpi=1',
    '--with-mpiexec=srun',
    '--with-netcdf=1',
    '--with-openmp=1',
    '--with-shared-libraries=1',
    '--with-zlib=1',
    'PETSC_ARCH=arch-linux-c-opt',
  ]
  configure.petsc_configure(configure_options)
  ```
