# Recipe: Patrick's Ubuntu desktop

## Install preprequisite tools

      sudo apt-get install hwloc

The following also assumes a working MPICH installation in `$HOME/code/petsc/arch-mpich-only`.

You also need CMake. I installed 3.19 myself, into `/usr`, using the installer on the CMake website.

## Obtain and build Kokkos (develop branch)

    cd $HOME/code
    git clone https://github.com/kokkos/kokkos -b develop
    cd kokkos
    mkdir build && cd build
    cmake \
      -DCMAKE_CXX_COMPILER=$HOME/code/petsc/arch-mpich-only/bin/mpicxx \
      -DCMAKE_INSTALL_PREFIX=$HOME/code/kokkos-install \
      -DKokkos_ENABLE_HWLOC=On \
      -DKokkos_ENABLE_OPENMP=On \
      ..
    make
    # make test  # Requires -DKokkos_ENABLE_TESTS=On
    make install

## Obtain and build Kokkos-kernels

    cd $HOME/code
    git clone https://github.com/kokkos/kokkos-kernels
    cd kokkos-kernels
    mkdir build && cd build
    cmake \
      -DCMAKE_CXX_COMPILER=$HOME/code/petsc/arch-mpich-only/bin/mpicxx \
      -DCMAKE_INSTALL_PREFIX=$HOME/code/kokkos-install \
      ..
    # make test  # Requires -DKokkosKernels_ENABLE_TESTS=On
    make install

## Obtain and build MARS

    cd $HOME/code
    git clone --recurse-submodules bitbucket:zulianp/mars -b stagbl
    cd mars
    mkdir build && cd build
    cmake  \
      -DKOKKOS_DIR=$HOME/code/kokkos-install \
      -DCMAKE_INSTALL_PREFIX=$HOME/code/mars-install \
      -DCMAKE_CXX_COMPILER=$HOME/code/petsc/arch-mpich-only/bin/mpicxx \
      -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
      -DTRY_WITH_KOKKOS=TRUE \
      -DCMAKE_VERBOSE_MAKEFILE=ON \
      -DCMAKE_BUILD_TYPE=Release \
      ..
    make install

## Run MARS demos

    cd $HOME/Downloads  # scratch directory

    $HOME/code/petsc/arch-mpich-only/bin/mpiexec -l \
      -n 4 \
      $HOME/code/mars-install/bin/mars_exec \
        -l 4 \
        -a matrix_free_poisson2D

    cd $HOME/Downloads  # scratch directory
    $HOME/code/petsc/arch-mpich-only/bin/mpiexec -l \
      -n 4 \
      $HOME/code/mars-install/bin/mars_exec \
        -l 4 \
        -a cstokes

    cd $HOME/Downloads  # scratch directory
    $HOME/code/petsc/arch-mpich-only/bin/mpiexec -l \
      -n 4 \
      $HOME/code/mars-install/bin/mars_exec \
        -l 4 \
        -a vstokes


## Clone a development branch of PETSc and configure and test with Kokkos/Kokkos-kernels

  Debug:

        cd $HOME/code
        git clone https://bitbucket.org/psanan/petsc -b psanan/stagbl-working-base-rebase  # volatile branch, often rebased!
        cd petsc-stagbl
        ./configure \
          --with-mpi-dir=$HOME/code/petsc/arch-mpich-only \
          --with-cuda=0 \
          --with-openmp \
          --with-hwloc \
          --download-fblaslapack \
          --download-suitesparse \
          --download-mumps \
          --download-scalapack \
          --download-metis \
          --download-parmetis \
          --download-mumps \
          --with-kokkos-dir=$HOME/code/kokkos-install \
          --with-kokkos-kernels-dir=$HOME/code/kokkos-install \
          PETSC_ARCH=arch-stagbl-kokkos-debug

  Optimized:

    Assuming working MPICH installation in $HOME/code/petsc/arch-mpich-only`:

        cd $HOME/code
        git clone https://bitbucket.org/psanan/petsc -b stagbl-working-base-rebase  # volatile branch, often rebased!
        cd petsc-stagbl
        ./configure \
          --with-mpi-dir=$HOME/code/petsc/arch-mpich-only \
          --with-cuda=0 \
          --with-openmp \
          --with-hwloc \
          --download-fblaslapack \
          --download-suitesparse \
          --download-mumps \
          --download-scalapack \
          --download-metis \
          --download-parmetis \
          --download-mumps \
          --with-kokkos-dir=$HOME/code/kokkos-install \
          --with-kokkos-kernels-dir=$HOME/code/kokkos-install \
          --with-debugging=0 \
          --COPTFLAGS=-g -O3 \
          --CXXOPTFLAGS=-g -O3 \
          --FOPTFLAGS=-g -O3 \
          --CUDAOPTFLAGS=-O3 \
          PETSC_ARCH=arch-stagbl-kokkos-opt


## Build PETSc DMStag tutorial example 4

    export PETSC_DIR=$HOME/code/petsc-stagbl
    export PETSC_ARCH=arch-stagbl-kokkos-debug # debug
    export PETSC_ARCH=arch-stagbl-kokkos-opt   # opt
    cd $PETSC_DIR/src/dm/impls/stag/tutorials
    make ex4

    ./ex4 -options_left -dim 2 -stag_grid_x 12 -stag_grid_y 12 -pc_type lu -pc_factor_mat_solver_type umfpack

    $HOME/code/petsc/arch-mpich-only/bin/mpiexec -np 4 \
      ./ex4 -options_left -dim 2 -stag_grid_x 12 -stag_grid_y 12 -pc_type lu -pc_factor_mat_solver_type mumps

## Run reference PETSc-native CPU case and examine output

    cd $HOME/code/petsc-stagbl/src/dm/impls/stag/tutorials

    rm -f *.vtr && ./ex4 -options_left -dim 2 -stag_grid_x 12 -stag_grid_y 12 -pc_type lu -pc_factor_mat_solver_type umfpack

    rm -f *.vtr && $HOME/code/petsc/arch-mpich-only/bin/mpiexec -np 4 \
      ./ex4 -options_left -dim 2 -stag_grid_x 12 -stag_grid_y 12 -pc_type lu -pc_factor_mat_solver_type mumps

    paraview ex4_element.vtr &   # requires working "paraview" and X tunnel, or copy files to local machine w/ paraview

  In Paraview, "apply" and view "Velocity (Averaged)" magnitude. Compare to this screenshot:

  ![image](images/ex4_2d_12x12_vel_mag.png)


## Run PETSc-native case, using Kokkos Vecs

    cd $HOME/code/petsc-stagbl/src/dm/impls/stag/tutorials


    rm -f *.vtr && ./ex4 -options_left -dim 2 -stag_grid_x 12 -stag_grid_y 12 -pc_type lu -pc_factor_mat_solver_type umfpack -dm_vec_type kokkos

    rm -f *.vtr && $HOME/code/petsc/arch-mpich-only/bin/mpiexec -np 4 \
      ./ex4 -options_left -dim 2 -stag_grid_x 12 -stag_grid_y 12 -pc_type lu -pc_factor_mat_solver_type mumps -dm_vec_type kokkos

    paraview ex4_element.vtr &   # requires working "paraview" and X tunnel, or copy files to local machine w/ paraview

  In Paraview, "apply" and view "Velocity (Averaged)" magnitude. Compare to this screenshot in the previous step.


Note that this doesn't prove that much, since the operator is still a normal Mat.

## Run MARS CPU/OpenMP-based equivalent

  The following assumes you have `PETSC_DIR` and `PETSC_ARCH` defined as above.

  This is just to test, and doesn't use the Trilionos solve!: (could update this with a build of Trilinos)

    cd /some/work/directory
    git clone https://gitlab.com/psanan/dmstag-mars-example  # this repository
    cd dmstag-mars-example
    make -j   \
        USE_NVCC_WRAPPER=0 \ 
        KOKKOS_DIR=$HOME/code/kokkos-install \
        USE_TRILINOS_SOLVER=0
    $HOME/code/petsc/arch-mpich-only/bin/mpiexec -l -np 4 ./ex4_mars


WIP: the above demo should be extended to the point of being able to compare the output!