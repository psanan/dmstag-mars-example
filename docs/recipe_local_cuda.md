# Recipe: Patrick's Ubuntu machine with CUDA

** DOES NOT WORK - ABANDONED BUT KEPT HERE FOR FUTURE REFERENCE **

Note that this attempts to do a completely separate install from the other instructions,
so that both can be tested side-by-side. This one uses "-2" as an identifier.

## Make sure hwloc exists

    sudo apt install hwloc

## Install a recent CMake

Follow the instructions on the CMake website. I installed 3.19 in `/usr` using their binary installer.

## Build Kokkos, with patch

** FAILS TESTS **

Relies on a patch taken from the `develop` branch. The Kokkos team responded to my [report][1] that `KEPLER30` didn't work anymore because of missing atomic operations.

    cd $HOME/code
    git clone https://github.com/kokkos/kokkos -b 3.3.01 kokkos-2
    cd kokkos-2
    wget https://github.com/kokkos/kokkos/commit/b968927fd70777f1ccdee1bb4914774937773fcb.patch
    git am b968927fd70777f1ccdee1bb4914774937773fcb.patch
    mkdir build-2 && cd build-2
    export PATH=$PATH:/usr/local/cuda/bin  # yuck!
    export NVCC_WRAPPER_DEFAULT_COMPILER=$HOME/code/petsc/arch-mpich-only/bin/mpicxx # YUCK
    cmake \
      -DCMAKE_CXX_COMPILER=$PWD/../bin/nvcc_wrapper \
      -DCMAKE_INSTALL_PREFIX=$HOME/code/kokkos-install-2 \
      -DKokkos_ENABLE_CUDA=On \
      -DKokkos_ARCH_KEPLER30=On \
      -DKokkos_ENABLE_CUDA_LAMBDA=On \
      -DKokkos_ENABLE_OPENMP=On \
      -DKokkos_ENABLE_HWLOC=On \
      ..
    # make && make test  # Requires -DKokkos_ENABLE_TESTS=On
    make install


Note that this currently fails some tests! `LastTestsFailed.log`:

    5:KokkosCore_UnitTest_Cuda2
    41:KokkosCore_PerfTestExec
    47:KokkosContainers_UnitTest_Cuda
    49:KokkosContainers_PerformanceTest_Cuda


## Build Kokkos-kernels

    cd $HOME/code
    git clone https://github.com/kokkos/kokkos-kernels -b 3.1.01 kokkos-kernels-2
    cd kokkos-kernels-2
    mkdir build-2 && cd build-2
    export PATH=$PATH:/usr/local/cuda/bin  # yuck!
    export NVCC_WRAPPER_DEFAULT_COMPILER=$HOME/code/petsc/arch-mpich-only/bin/mpicxx # YUCK
    cmake \
      -DCMAKE_CXX_COMPILER=$HOME/code/kokkos-install-2/bin/nvcc_wrapper \
      -DCMAKE_INSTALL_PREFIX=$HOME/code/kokkos-install-2 \
      ..
    # make && make test  # Requires -DKokkosKernels_ENABLE_TESTS=On
    make install

Very slow! (20+ minutes)

## Obtain and build MARS

    mkdir -p $HOME/code
    cd $HOME/code
    git clone --recurse-submodules https://bitbucket.org/zulianp/mars -b stagbl
    cd mars
    mkdir build-2 && cd build-2
    export PATH=$PATH:/usr/local/cuda/bin  # yuck!
    export NVCC_WRAPPER_DEFAULT_COMPILER=$HOME/code/petsc/arch-mpich-only/bin/mpicxx # YUCK
    cmake \
      -DKOKKOS_DIR=$HOME/code/kokkos-install-2 \
      -DCMAKE_INSTALL_PREFIX=$HOME/code/mars-install-2 \
      -DCMAKE_CXX_COMPILER=$HOME/code/kokkos-install-2/bin/nvcc_wrapper \
      -DCMAKE_CXX_FLAGS="-w -expt-extended-lambda --expt-relaxed-constexpr" \
      -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
      -DTRY_WITH_KOKKOS=TRUE \
      -DMARS_USE_CUDA=ON \
      -DMARS_NO_RDMA=ON\
      -DCMAKE_VERBOSE_MAKEFILE=ON \
      -DCMAKE_BUILD_TYPE=Release \
      ..
    make install

(Note sure if adding the extended lambda flag indicates that I did something wrong)


## Run MARS demos

    cd $HOME/Downloads  # scratch directory

    $HOME/code/petsc/arch-mpich-only/bin/mpiexec -l \
      -n 4 \
      $HOME/code/mars-install/bin/mars_exec \
        -l 4 \
        -a matrix_free_poisson2D

    cd $HOME/Downloads  # scratch directory
    $HOME/code/petsc/arch-mpich-only/bin/mpiexec -l \
      -n 4 \
      $HOME/code/mars-install/bin/mars_exec \
        -l 4 \
        -a cstokes

    cd $HOME/Downloads  # scratch directory
    $HOME/code/petsc/arch-mpich-only/bin/mpiexec -l \
      -n 4 \
      $HOME/code/mars-install/bin/mars_exec \
        -l 4 \
        -a vstokes


## PETSc

  WIP:
  This runs into configure problems.. horrible hack is to replace the include checks in `kokkos.py` and `kokkos-kernels.py` with `stdio.h` checks.

  Debug:

        cd $HOME/code
        git clone https://bitbucket.org/psanan/petsc -b psanan/stagbl-working-base-rebase  # volatile branch, often rebased!
        cd petsc-stagbl
        # Perform horrible hack (see above)
        ./configure \
          --with-mpi-dir=$HOME/code/petsc/arch-mpich-only \
          --with-cuda=1 \
          --with-cuda-dir=/usr/local/cuda \
          --with-openmp \
          --with-hwloc \
          --download-fblaslapack \
          --download-suitesparse \
          --download-mumps \
          --download-scalapack \
          --download-metis \
          --download-parmetis \
          --download-mumps \
          --with-kokkos-dir=$HOME/code/kokkos-install-2 \
          --with-kokkos-kernels-dir=$HOME/code/kokkos-install-2 \
          PETSC_ARCH=arch-stagbl-kokkos-2-debug

Tests fail because `cuda` Vec type isn't recognized (bad but not blocking, here).


## PETSc DMStag tutorial example 4

    export PETSC_DIR=$HOME/code/petsc-stagbl
    export PETSC_ARCH=arch-stagbl-kokkos-2-debug # debug
    cd $PETSC_DIR/src/dm/impls/stag/tutorials
    make ex4

    ./ex4 -options_left -dim 2 -stag_grid_x 12 -stag_grid_y 12 -pc_type lu -pc_factor_mat_solver_type umfpack

    $HOME/code/petsc/arch-mpich-only/bin/mpiexec -np 4 \
      ./ex4 -options_left -dim 2 -stag_grid_x 12 -stag_grid_y 12 -pc_type lu -pc_factor_mat_solver_type mumps

    rm -f *.vtr && ./ex4 -options_left -dim 2 -stag_grid_x 12 -stag_grid_y 12 -pc_type lu -pc_factor_mat_solver_type umfpack -dm_vec_type kokkos

    rm -f *.vtr && $HOME/code/petsc/arch-mpich-only/bin/mpiexec -np 4 \
      ./ex4 -options_left -dim 2 -stag_grid_x 12 -stag_grid_y 12 -pc_type lu -pc_factor_mat_solver_type mumps -dm_vec_type kokkos

## MARS example

WIP:
* Make sure to supply vars to use -2 variants of kokkos and mars
* Need to set CXX to be the nvcc_wrapper
* Need to extend CXX flags as above
* Need the equivalent of these flags with it:
      -DCMAKE_CXX_FLAGS="-w -expt-extended-lambda --expt-relaxed-constexpr"


[1]: https://github.com/kokkos/kokkos/pull/3780
