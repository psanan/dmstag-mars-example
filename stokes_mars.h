#ifndef STOKES_MARS_H_
#define STOKES_MARS_H_

#include <petscdm.h>

#include "ex4_ctx.h"

#ifdef __cplusplus
extern "C" {
#endif
  PetscErrorCode main_MARS(DM,Vec*,Ctx);
  PetscErrorCode MARSFinalize();
  PetscErrorCode MARSInitialize(int, char**);
#ifdef __cplusplus
}
#endif

#endif
